<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'desafiodaordem');

/** MySQL database password */
define('DB_PASSWORD', 'desafiodaordem');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O OzjisMc_YBbJH*9eXJ~NY+4KDJX,Ob&0nnyhu>|d=<VAaE`*X(;ME{$>.kP*Dj');
define('SECURE_AUTH_KEY',  'ws.rrWIlJINK5nsT0Y(Hj7sC~r9ZCnUWU[n:!H7zLn#}X]lKy>TJuoH+{-gcZw+n');
define('LOGGED_IN_KEY',    '0pv:h6;Zc -1 7YA@.=H]xW|*p%sh-Et|6T`=6W5;Ll-jt7[I1Ib8Lvm||#F$UqU');
define('NONCE_KEY',        'FFByb9.(2z@^EZd$R1&dY^pR{a#/^K2<{tTLk3.BPzRjT(@bx.}uW9=<1e&7!Q{,');
define('AUTH_SALT',        'Ok<Hxr[ys|Sa,zmWx?Cf0^@(R-!g^8gv8 nIO|EFj*Px1F}K=IjF$_l)(Rjg?>uy');
define('SECURE_AUTH_SALT', 'FblA`i)G`t<5Q<^Pgw`jp/N=AYoYueJ.cM7gh8_o_g9b1`cky3E~y#}-w|YDZ}D?');
define('LOGGED_IN_SALT',   'g.9+>+SVs&?G$$#z-xK!T8d!Rl0$$Q^6)SgxeximeZ+:|u^>TwSX)6X3dJ830|4G');
define('NONCE_SALT',       'r&sww=/Qcp;hla@}{)GY `|+^{1U;)!dw#IUc+_B]n?UF*!S;m|n9Q-mX*%U(pl1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
